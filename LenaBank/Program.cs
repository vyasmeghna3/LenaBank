﻿/* ******************************************************************************************************************************************************
 * Project Name:    LenaBank
 * Author:          Meghna Vyas(MV)
 * 
 * File:            Program.cs
 * 
 * Description:     
 *   A C# program to implement working of a bank.
 *   Contains classes :
 *      Program -- main class
 *      Account -- basic account details and functions
 *      SavingsAccount -- for a Savings account in the bank
 *      CurrentAccount -- for a current account in Lena Bank
 *   
 * Revision History:   
 *  2018-Jun-15 : Project created (MV)
 *  2018-Jun-15 : Created SavingsAccount class (MV)
******************************************************************************************************************************************************* */

/*
 * System namespace is used
 * */
using System;


namespace LenaBank
{
    /*Program class contains main method. Creates Accounts for customers.
     * */
    class Program
    {
        /*class contains main method that creates accounts of customers in bank.
         * */
        static void Main(string[] args)
        {
            /* //array of objects created
             Account[] ac = new Account[4];
             ac[0] = new SavingsAccount("Meghna", 5000);
             ac[1] = new SavingsAccount("Siddhi", 2000);
             ac[2] = new CurrentAccount("Shivani", 5000);
             ac[3] = new CurrentAccount("Miggu", 3000);

             //for loop to print information stored accounts
             for(int i=0;i<4;i++)
             {
                 ac[i].Print();
                 Console.WriteLine("\n");
             }

             Account ac1 = new Account("Geeta", 5500);
             SavingsAccount sa = new SavingsAccount("Manoj", 4000);


             //method added to delegate using event
             ac1.OnBalanceChange += SendSMS;

             ac1.Withdraw(500);
             ac1.PrintPassbook();

             sa.Withdraw(600);

             //ca.OnBalanceChange += SendSMS;

             void SendSMS(int an, double newBal)
             {
                 Console.WriteLine("\t--SMS RECIEVED!!--\n Balanced Updated! New balance is Rs.{0}", newBal);
             }
             */
            
            SavingsAccount sa = new SavingsAccount("Meghna", 4000);
            sa.Withdraw(200, out double Bal);
            Console.WriteLine("New balance = {0}", Bal);
            try
            {
                sa.Withdraw(5000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
