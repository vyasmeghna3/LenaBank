﻿/* ******************************************************************************************************************************************************
 * Project Name:    LenaBank
 * Author:          Meghna Vyas(MV)
 * 
 * File:            Account.cs
 * 
 * Description:     
 *   A C# program to implement working of a bank.
 *   Contains classes :
 *      Program -- main class
 *      Account -- basic account details and functions
 *      SavingsAccount -- for a Savings account in the bank
 *      CurrentAccount -- for a current account in Lena Bank
 *   
 * Revision History:   
 *  2018-Jun-15 : Project created (MV)
******************************************************************************************************************************************************* */

/*
 * System namespace is used
 * */
using System;
using System.Collections.Generic;

namespace LenaBank
{
    /*Account class contains the basic details of an account.
     * Contains fields :
     *      readonly int accountNumber -- holds the customer's acc no, cannot be changed by user
     *      string holdersName -- holds customer's name
     *      double balance -- holds the account's current balance
     *      static int lastNumber -- holds the last assigned account no.
     * Contains methods :
     *      void Deposit() -- deposits an amount into the account
     *      void Withdraw() -- withdraws an amount from the account
     *      */
    class Account
    {
        /*delegate created to check on change in balance
         * */
        public delegate void BalanceChangeDelegate(int an, double newBal);

        /*event to fire the delegate
         * */
        public event BalanceChangeDelegate OnBalanceChange;

        List<Transaction> al = new List<Transaction>();



       


        readonly int accountNumber;
        string holdersName;
        protected double balance;
        public double Balance { get { return balance; }
            protected set
            { balance = value;
                if(OnBalanceChange != null)
                OnBalanceChange(accountNumber, balance); } }
        static int lastNumber=0;

        /*parameterized class constructor initializes fields, increments lastNumber and sets accountNumber
         * */
         public Account(string hn, double b)
        {
            holdersName = hn;
            balance = b;
            accountNumber += ++lastNumber;
        }

        /*
         * takes amount from user and adds it the current balance.
         * */
        public virtual void Deposit(int amount)
        {
            Balance += amount;
            Transaction tr = new Transaction('d', amount, Balance);
            al.Add(tr);
                 if (OnBalanceChange != null)
                OnBalanceChange(accountNumber, balance);
        }

        /*
         * takes withdrawal amount from user and reduces it from the current balance.
         * */
        public virtual void Withdraw(double amount)
        {
            Balance -= amount;
            Transaction tr = new Transaction('w', amount, Balance);
            al.Add(tr);
            if (OnBalanceChange != null)
                OnBalanceChange(accountNumber, balance);
        }

        /*prints all the details of the account
         * */
        public virtual void Print()
        {
            Console.WriteLine("Account NO : {0}", accountNumber);
            Console.WriteLine("Holder's Name : {0}", holdersName);
            Console.WriteLine("Current Balance : Rs.{0}", balance);

        }

        public void PrintPassbook()
        {
            foreach (Transaction obj in al)
            {
                Console.WriteLine("Account Number : {0}", accountNumber);
                Console.WriteLine("Holder's Name : {0}", holdersName);
                Console.WriteLine("TransactionNumber : {0}",obj.TransactionNumber);
                Console.WriteLine("Date of Transaction : {0}", obj.Date);
                Console.WriteLine("Transaction type : {0}", obj.TransactionType);
                Console.WriteLine("Amount : {0}", obj.Amount);
                Console.WriteLine("Current Balance : {0}", obj.Balance);
            }
        }




          
        
    }
}
